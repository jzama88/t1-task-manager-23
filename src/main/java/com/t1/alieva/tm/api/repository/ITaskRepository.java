package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @Nullable String userId,
            @NotNull String name) throws
            AbstractEntityNotFoundException;

    @Nullable
    Task create(
            @Nullable String userId,
            @NotNull String name,
            @NotNull String description) throws
            AbstractEntityNotFoundException;

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId);
}
