package com.t1.alieva.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getName() {
        return "about";
    }

    @Override
    @NotNull
    public String getArgument() {
        return "-a";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name:Alieva Djamilya");
        System.out.println("E-mail:jzama88@gmail.com");
    }
}
